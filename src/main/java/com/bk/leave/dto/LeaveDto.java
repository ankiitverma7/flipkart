package com.bk.leave.dto;

import java.io.Serializable;
import java.util.Date;

public class LeaveDto implements Serializable {

	
	private int employeeId;
	private Date startDate;
	private Date endDate;
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
